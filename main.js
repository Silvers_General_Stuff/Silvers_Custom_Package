/**
 * Testing if the module is working as planned
 *
 * @returns {string} This is a successful test
 */
function moduleTest () {
  return 'This is a successful test'
}

/**
 * Cleans up a query object to work with my apis
 *
 * @param {object} query to clean up
 * @returns {object} cleaned query
 */
function cleanQuery (query) {
  let keys = Object.keys(query)
  for (let i = 0; i < keys.length; i++) {
    if (!isNaN(query[keys[i]] - 0)) {
      query[keys[i]] = query[keys[i]] - 0
    }
    if (query[keys[i]] === 'true') {
      query[keys[i]] = true
    }
    if (query[keys[i]] === 'false') {
      query[keys[i]] = false
    }
  }
  delete query.limit
  delete query.skip
  delete query.beautify
  delete query.livecount
  delete query.site
  return query
}

/**
 * Converts date to another format
 *
 * @param {string} inputFormat ISO date format
 * @param {string} format what you want (european/american)
 * @returns {string} converted date
 */
function convertDate (inputFormat, format) {
  if (typeof format === 'undefined') { return inputFormat }
  let currentDate = new Date(inputFormat)
  let mm = currentDate.getMonth() + 1
  mm = (mm < 10) ? '0' + mm : mm
  let dd = currentDate.getDate()
  dd = (dd < 10) ? '0' + dd : dd
  let yyyy = currentDate.getFullYear()

  if (format === 'American') {
    return mm + '/' + dd + '/' + yyyy
  }
  if (format === 'European') {
    return dd + '/' + mm + '/' + yyyy
  }
  return inputFormat
}

/**
 * Checks if a particular string is json or not
 *
 * @param {object} item that you want to test
 * @returns {boolean} if the input is json or not
 */
function IsJsonString (item) {
  item = typeof item !== 'string' ? JSON.stringify(item) : item
  try {
    item = JSON.parse(item)
  } catch (e) {
    return false
  }
  return typeof item === 'object' && item !== null
}

/**
 * Just capitalises the first letter of a string
 *
 * @param {string} type category that the particular request belongs to
 * @param {object} requestsObject input object
 * @returns {object} same format as the input object
 */
function bumpRequests (type, requestsObject) {
  requestsObject.requests.count += 1
  let time0 = new Date().toISOString()
  time0 = time0.slice(0, 16) + ':00Z'
  if (typeof requestsObject.requests.times[time0] === 'undefined') {
    requestsObject.requests.times[time0] = { total: 0 }
  }
  if (typeof requestsObject.requests.times[time0][type] === 'undefined') {
    requestsObject.requests.times[time0][type] = 0
  }
  requestsObject.requests.times[time0].total += 1
  requestsObject.requests.times[time0][type] += 1
  return requestsObject
}

/**
 * Just capitalises the first letter of a string
 *
 * @param {string} str imput string
 * @returns {string} capitalised string.
 */
function capitalize (str) {
  if (typeof str !== 'string') { return '' }
  return str.charAt(0).toUpperCase() + str.slice(1)
}

/**
 * Creates a random string of specified length
 *
 * @param {number} [length=50] Input number
 * @returns {string} random string.
 */
function createRandomString (length) {
  if (typeof length === 'undefined') { length = 50 }
  return [...Array(length)].map(() => (~~(Math.random() * 36)).toString(36)).join('')
}

/**
 * Converts a number to g/s/c format
 *
 * Mostly used for GuildWars2
 *
 * @param {number} number Input number
 * @param {boolean} shorten cut off the trailing currency or not
 * @returns {string} formatted g/s/c.
 */
function convertToGold (number, shorten) {
  let input = Math.floor(number).toString()
  let minus = ''
  if (input.includes('-')) {
    minus = '-'
  }
  input = input.replace('-', '')
  let copper = input.slice(-2) + 'c'
  let silver = input.slice(-4, -2) + 's'
  let gold = input.slice(0, -4) + 'g'

  if (silver.length === 1) {
    silver = ''
  }
  if (gold.length === 1) {
    gold = ''
  }

  let result = minus + gold + silver + copper
  if (shorten) {
    if (number >= 100) {
      // remove trailing copper
      result = result.replace('00c', '')
    }

    if (number >= 10000) {
      // remove trailing silver
      result = result.replace('00s', '')
    }
  }
  return result
}

/**
 * This beautifies json depending on teh flag
 *
 * @param {string} json Input json
 * @param {string} flag Flag to expand it out or not (min/human)
 * @returns {string} Resulting json.
 */
function beautifyJSON (json, flag) {
  if (flag === 'min') {
    return JSON.stringify(json)
  } else {
    return JSON.stringify(json, null, '  ')
  }
}

/**
 * Function to sort array, need to pass in the strings directly
 *
 * @param {string} a
 * @param {string} b
 * @param {boolean} reverse
 * @returns {number} Resulting order.
 */
function sorter (a, b, reverse) {
  if (reverse) {
    if (a < b) { return 1 }
    if (a > b) { return -1 }
  } else {
    if (a < b) { return -1 }
    if (a > b) { return 1 }
  }
  return 0
}

/**
 * Returns a number from a  string
 *
 * Strips out any non number characters and forces conversion
 *
 * @param {number} input
 * @returns {number} Resulting number.
 */
function stringToNumber (input) {
  if (typeof input === 'undefined') { input = 0 }
  return input.toString().replace(/[^\d.-]/g, '') - 0 || 0
}

/**
 * Returns an array of unique objects
 *
 * @param {array} array of the account
 * @returns {array} Unique items.
 */
function uniq (array) {
  let seen = {}
  return array.filter((item) => { return seen.hasOwnProperty(item) ? false : (seen[item] = true) })
}

/**
 * For Datawars2
 *
 * Gets the next update based on the users level
 *
 * @param {string} level of the account
 * @param {boolean} newAccount is it a new account
 * @param {object} mapping an object mapping times to levels
 * @returns {string} ISO datestamp.
 */
function getNextUpdate (level, newAccount, mapping) {
  let randomHour = 0
  let randomMinute = 0
  if (newAccount) {
    randomHour = Math.floor(Math.random() * 23)
    randomMinute = Math.floor(Math.random() * 59)
  }
  return new Date(new Date().getTime() + (((mapping[level] - randomHour) * 60) - randomMinute) * 60000).toISOString()
}

/**
 * This function is a filter function for all numbers
 *
 * Strips all non number characters
 *
 * @param {string} item item being filtered
 * @param {string} filter what the item is being filtered by
 * @param {number} multiplier what to multiply the filter quanty by
 * @returns {boolean} returns true or false.
 */
function filterGeneralNumber (item, filter, multiplier) {
  if (typeof multiplier === 'undefined') { multiplier = 1 }
  let start = 0
  let end = Infinity

  let split = filter.split(',')
  if (split.length > 0) { start = (split[0] - 0) * multiplier }
  if (split.length > 1) { end = (split[1] - 0) * multiplier }
  if (end < start) { end = Infinity }

  let converted = item.toString().replace(/[^\d.-]/g, '') - 0 || 0
  return converted >= start && converted <= end
}

/**
 * This function is a filter function text
 *
 * Converts all inputs to lowercase
 *
 * @param {string} item item being filtered
 * @param {string} filter what the item is being filtered by
 * @returns {boolean} returns true or false.
 */
function filterGeneralText (item, filter) {
  if (typeof item === 'undefined') { return false }
  if (typeof filter === 'undefined') { return false }
  item = item.toString().toLowerCase()
  filter = filter.toString().toLowerCase()
  return item.indexOf(filter) !== -1
}

// these are functions that require external dependencies

/**
 * This function gets the specified url with options
 *
 * Requires:
 * * request-promise-native
 * @param {object} rp request-promise-native object
 * @param {string} url url ye are requesting
 * @param {object} [options] optional options
 * @returns {object} with headers, body and errors.
 */
function getURL (rp, url, options) {
  if (typeof options === 'undefined') {
    options = { uri: url }
  }
  if (typeof options.uri === 'undefined') {
    options.uri = url
  }
  options.resolveWithFullResponse = true
  let result = { headers: undefined, body: undefined, error: undefined }
  return rp(options)
    .then((response) => {
      result.headers = response.headers
      result.body = response.body
      return result
    })
    .catch((error) => {
      result.error = error
      return result
    })
}

/**
 * Verifies sessionKey
 *
 * Requires:
 * * mongodb
 *
 *
 * @param {object} db Mongodb instance
 * @param {string} [collection=accounts_sessions] Collection that you want to store the session data in
 * @param {string} session user that the key is being generated for
 * @param {object} req request object
 * @returns {string} sessionKey.
 */
async function verifySession (db, collection, session, req) {
  if (typeof collection === 'undefined') { collection = 'accounts_sessions' }
  await db.collection(collection).createIndex({ 'session': 1 })
  let existing = await db.collection(collection).find({ session: session }).project({ _id: 0 }).toArray()
  let result = {}
  if (existing.length > 0) {
    result.result = 'success'
    result.success = existing[0].user

    let tmp = {}
    tmp.lastAccessed = new Date().toISOString()
    tmp.logins = existing[0].logins + 1
    tmp.userAgent = req.headers['user-agent']
    tmp.ip = req.headers['x-forwarded-for']
    await db.collection(collection).updateOne({ session: session }, { $set: tmp }, { upsert: true })
  } else {
    result.result = 'error'
    result.error = 'Invalid Session'
  }
  return result
}

/**
 * Creates a sessionKey
 *
 * Requires:
 * * mongodb
 *
 * @param {object} db Mongodb instance
 * @param {string} [collection=accounts_sessions] Collection that you want to store the session data in
 * @param {string} user user that the key is being generated for
 * @param {object} req request object
 * @param {string} [sessionKey] sessionKey override used for testing
 * @returns {string} sessionKey.
 */
async function createUniqueSession (db, collection, user, req, sessionKey) {
  if (typeof collection === 'undefined') { collection = 'accounts_sessions' }
  if (typeof sessionKey === 'undefined') { sessionKey = createRandomString(145) }
  let existing = await db.collection(collection).find({ session: sessionKey }).project({ _id: 0 }).toArray()
  if (existing.length >= 1) {
    sessionKey = await createUniqueSession(db, collection, user, req)
  }
  let tmp = {}
  tmp.session = sessionKey
  tmp.user = user
  tmp.firstAccessed = new Date().toISOString()
  tmp.lastAccessed = new Date().toISOString()
  tmp.logins = 1
  tmp.userAgentInitial = req.headers['user-agent']
  tmp.userAgent = req.headers['user-agent']
  tmp.ip = req.headers['x-forwarded-for']

  await db.collection(collection).updateOne({ session: sessionKey }, { $set: tmp }, { upsert: true })

  return sessionKey
}

/**
 * This function gets sends a page view to google analytics
 *
 * Requires:
 * * universal-analytics
 * * uuid-by-string
 *
 * @param {object} ua universal-analytics
 * @param {object} uuid uuid-by-string
 * @param {object} req page request object
 * @param {string} ga the GA code
 * @returns {boolean}
 */
function sendPageView (ua, uuid, req, ga) {
  let currentUuid = uuid(req.userAgent() + req.headers['accept-language'] + req.headers['x-forwarded-for'])
  let pageView = {}

  pageView.dp = req.href()
  pageView.ua = req.userAgent()
  pageView.uip = req.headers['x-forwarded-for']

  ua(ga, currentUuid, { https: true })
    .pageview(pageView)
    .send()
  return true
}

/**
 * Logs the specified message into the DB
 *
 * Requires:
 * * mongodb
 *
 * @param {object} db mongodb object
 * @param {string} [logCollection] collection to put it into
 * @param {string} type category
 * @param {string} location subcategory
 * @param {string} message main message
 * @param {object} [misc] object that contains other messages
 * @param {boolean} [consoleFlag] log to console
 * @param {number} [maxSize] size of collection
 * @returns {null}
 */
async function logToDB (db, logCollection, type, location, message, misc, consoleFlag, maxSize) {
  if (typeof logCollection === 'undefined') { logCollection = 'logging' }
  let tmp = {
    time: new Date().toISOString(),
    type: type,
    location: location,
    message: message
  }

  let size = 52428800
  if (typeof maxSize === 'number') {
    // converts it to bytes
    size = 51200 * maxSize
  }

  if (typeof misc !== 'undefined') {
    let keys = Object.keys(misc)
    for (let i = 0; i < keys.length; i++) {
      tmp[keys[i]] = misc[keys[i]]
    }
  }
  if (consoleFlag) {
    let miscTmp
    if (typeof misc === 'undefined') {
      miscTmp = {}
    } else {
      if (typeof misc !== 'object') {
        miscTmp = {}
      } else {
        miscTmp = JSON.stringify(misc)
      }
    }

    console.log(new Date().toISOString(), type, location, message, miscTmp)
  }
  // creates collection if not exist
  await db.createCollection(logCollection, { capped: true, size: size })
  // logs to console if itself has an issue
  await db.collection(logCollection).insertOne(tmp)
}

// these are ones  I will have to refactor soon
function querySorter (query, active, endpoint, defaultFields, defaultFind) {
  let result = {}
  if (active.beautify) {
    result.beautify = query.beautify || 'human'
  }

  result.fields = query.fields || defaultFields || 'id,name,lastUpdate,buy_quantity,buy_price,sell_quantity,sell_price'

  result.project = { _id: 0 }
  if (active.project) {
    let fieldsArray = result.fields.split(',')
    for (let i = 0; i < fieldsArray.length; i++) {
      result.project[fieldsArray[i]] = 1
    }
  }

  result.find = { $and: [] }

  if (active.ids) {
    let ids = []
    if (typeof query.ids !== 'undefined') {
      ids = query.ids.split(',')
    }
    let idQuery = { $or: [] }
    for (let i = 0; i < ids.length; i++) {
      idQuery.$or.push({ 'id': ids[i] - 0 })
    }
    if (idQuery.$or.length > 0) {
      result.find.$and.push(idQuery)
    }
  }

  if (active.itemID) {
    let ids = []
    if (typeof query.itemID !== 'undefined') {
      ids = query.itemID.split(',')
    }
    let idQuery = { $or: [] }
    for (let i = 0; i < ids.length; i++) {
      idQuery.$or.push({ 'itemID': ids[i] - 0 })
    }
    if (idQuery.$or.length > 0) {
      result.find.$and.push(idQuery)
    }
  }

  if (active.start) {
    if (typeof query.start !== 'undefined') {
      result.find.$and.push({ date: { $gte: new Date(query.start).toISOString() } })
    }
  }
  if (active.end) {
    if (typeof query.end !== 'undefined') {
      result.find.$and.push({ date: { $lte: new Date(query.end).toISOString() } })
    }
  }

  if (active.filter) {
    if (result.find.$and.length === 0) {
      if (typeof query.filter !== 'undefined') {
        result.find = filterSorter(query.filter)
      }
    } else {
      if (typeof query.filter !== 'undefined') {
        let filterResult = filterSorter(query.filter)
        let keys = Object.keys(filterResult)
        for (let i = 0; i < keys.length; i++) {
          let tmp = {}
          tmp[keys[i]] = filterResult[keys[i]]
          result.find.$and.push(tmp)
        }
      }
    }
  }

  if (typeof result.find.$and !== 'undefined' && result.find.$and.length === 0) {
    result.find = {}
    if (typeof defaultFind === 'object') {
      result.find = defaultFind
    }
  }
  return result
}

function filterSorter (filterQuery) {
  let filter = {}
  if (filterQuery.length >= 3 && filterQuery.indexOf(':') !== -1) {
    let splitFilter = filterQuery.split(',')
    let fieldObject = {}
    if (typeof splitFilter === 'object') {
      for (let i = 0; i < splitFilter.length; i++) {
        if (splitFilter[i].length >= 3) {
          let field = splitFilter[i]
          let c = field.split(':')
          let searchTerm = c[1]
          let tmp = {}
          // boolean sorting
          if (searchTerm === 'TRUE' || searchTerm === 'FALSE') {
            if (searchTerm === 'TRUE') {
              searchTerm = true
            }
            if (searchTerm === 'FALSE') {
              searchTerm = false
            }
          }
          // handing numerical operators
          if (searchTerm === 'gt' || searchTerm === 'gte' || searchTerm === 'lt' || searchTerm === 'lte' || searchTerm === 'eq' || searchTerm === 'ne') {
            tmp['$' + searchTerm] = c[2] - 0
            searchTerm = tmp
          }
          // handing array search
          if (searchTerm === 'in' || searchTerm === 'nin') {
            tmp['$' + searchTerm] = c[2].split('.')
            searchTerm = tmp
          }
          // handling regex search
          if (searchTerm === 'cts') {
            tmp['$regex'] = c[2]
            tmp['$options'] = 'x'
            searchTerm = tmp
          }

          fieldObject[c[0]] = searchTerm
        }
      }
    }
    filter = fieldObject
  }
  return filter
}

/**
 * Takes an array of objects and returns an object to insert them into a database
 *
 * @param {array} data array of objects
 * @param {number} [limit] maximum amount to insert in one go
 * @param {string} [accessor] accessor for the find query
 * @param {object} [setOnInsert] accessor for the find query
 * @returns {object}
 */
function dataToDbArray (data, limit, accessor, setOnInsert) {
  let tmp = { arrays: 0, 0: [] }
  if (typeof limit === 'undefined') { limit = 25000 }
  if (typeof accessor === 'undefined') { accessor = 'id' }
  for (let j = 0; j < data.length; j++) {
    let temp = {
      updateOne: {
        update: { $set: data[j] },
        upsert: true
      }
    }
    temp.updateOne.filter = {}
    temp.updateOne.filter[accessor] = data[j][accessor]
    if (typeof setOnInsert !== 'undefined' && typeof setOnInsert === 'object') {
      temp.updateOne.update.$setOnInsert = setOnInsert
    }
    if (tmp[tmp.arrays].length < limit) {
      tmp[tmp.arrays].push(temp)
    } else {
      tmp.arrays += 1
      tmp[tmp.arrays] = [temp]
    }
  }
  return tmp
}

module.exports = {
  moduleTest: moduleTest,
  filterGeneralText: filterGeneralText,
  filterGeneralNumber: filterGeneralNumber,
  getNextUpdate: getNextUpdate,
  uniq: uniq,
  stringToNumber: stringToNumber,
  sorter: sorter,
  beautifyJSON: beautifyJSON,
  convertToGold: convertToGold,
  createRandomString: createRandomString,
  capitalize: capitalize,
  bumpRequests: bumpRequests,
  IsJsonString: IsJsonString,
  convertDate: convertDate,
  cleanQuery: cleanQuery,
  sendPageView: sendPageView,
  getURL: getURL,
  createUniqueSession: createUniqueSession,
  verifySession: verifySession,
  logToDB: logToDB,
  filterSorter: filterSorter,
  querySorter: querySorter,
  dataToDbArray: dataToDbArray
}
